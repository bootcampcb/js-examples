"use strict";

var customerId;
var custFirstName = "Chuck";
var custMiddleName;
var custLastName;
let custGender;
var custDOB;
var custDriverLicense;
var custAutoPolicyNumber;

console.log("First Name", custFirstName);
console.log("Last Name " + custLastName);

custDOB = new Date();  
custDOB.setFullYear(2011, 6, 1);

console.log("Date" , custDOB);
console.log("Date", custDOB.toDateString());
console.log("Date", custDOB.toLocaleDateString());
