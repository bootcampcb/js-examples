"use strict";
function processInput() {
    const nameField = document.getElementById("nameInput");
    const hobbyField = document.getElementById("hobbyInput");
    const colorField = document.getElementById("colorInput");
    const maxCupsField = document.getElementById("maxCupsInput");
    const currentCupsField = document.getElementById("currentCupsInput");
    const myPara = document.getElementById("myParagraph");
    
    let message;
    let cupsLeft;
    cupsLeft = maxCupsField.value - currentCupsField.value;
    message = `Hi my name is ${nameField.value}. I like to ${hobbyField.value} and my favorite color is ${colorField.value}.  I limit myself to ${maxCupsField.value} of coffee and currently have had ${currentCupsField.value}. You have ${cupsLeft} cups left.`;
    
    myPara.innerHTML = message;
    myPara.style.color = colorField.value;
}

processInput();