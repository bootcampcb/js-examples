
"use strict";

let objSet1 = document.querySelectorAll("p");
let objSet2 = document.querySelectorAll(".attn");
let objSet3 = document.querySelectorAll("p.attn");
let objSet4 = document.querySelectorAll("img[alt]");
let objSet5 = document.querySelectorAll("div > p");
let objSet6 = document.querySelectorAll("div, span");
let objSet7 = document.querySelector("img:not([alt])");

window.onload = init;

function init() {
    const msgDivs = document.getElementsByClassName("imageGroup");
    // for (let i = 0; i < msgDivs.length; i++) {
    //     msgDivs[i].style.border = "8px solid red";
    // }

    Array.from(msgDivs).forEach((element) => {
        element.style.border = "8px solid red";
    });

    Array.from(objSet7).forEach((element) => {
        element.alt = "graffiti image";
    });

}


