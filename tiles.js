var length = 30;
var width = 10;
var tilesToOrder = getTiles(length, width);

function getTiles(l, w) {
    let tilesPerBox = 15;
    let area = l * w;
    let tilesNeeded = area * 1.10;
    let boxesNeeded = tilesNeeded / tilesPerBox;

    return Math.ceil(boxesNeeded);
}

console.log(tilesToOrder);