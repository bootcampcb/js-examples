function parseAndDisplayName (userName) {
    let pos1 = userName.indexOf(" ");
    let pos2 = userName.lastIndexOf(" ");

    if (pos1 == -1) {
        console.log("Name: " + userName);
    }
    else if (pos1 == pos2) {
        let firstName = userName.substr(0, pos1);
        let lastName = userName.substring(pos1+1)
    
        console.log("First Name: " + firstName + "  Last Name: " + lastName);
    }
    else {
        let firstName = userName.substr(0, pos1);
        let middleName = userName.substr(pos1,pos2);
        let lastName = userName.substring(pos2+1);
    
        console.log("First Name: " + firstName + "  Middle Name: " + middleName + "  Last Name: " + lastName)  
    }


}

parseAndDisplayName("Cher");
parseAndDisplayName("Brenda Kaye");
parseAndDisplayName("Dana Lynn Wyatt");