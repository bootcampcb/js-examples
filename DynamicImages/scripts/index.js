
"use strict";
let imageFiles = [
    {name: "images/dogloggingin.jpg", description: "Dog logging in"},
    {name: "images/dogsplaying.jpg", description: "Dogs playing"},
    {name: "images/largedogbed.webp", description: "Large dog in bed"},
    {name: "images/smalldogbed.webp", description: "Small dog in bed"},
    {name: "images/oldsheena.jpg", description: "My old dog"}
];
window.onload = init;

function init() {
    const addImage = document.getElementById("addImage");
    const clearImages = document.getElementById("clearImages");

    addImage.onclick = onAddImageClicked;
    clearImages.onclick = onclearImagesClicked;

    const dogSelect = document.getElementById("dogs");
    for (let i = 0; i < imageFiles.length; i++) {
        let theOption =  new Option(imageFiles[i].description, undefined);
        dogSelect.appendChild(theOption);

    }
}
function onAddImageClicked() {
    const myDiv = document.getElementById("myDiv");
    const dogSelect = document.getElementById("dogs");

    let theImage = imageFiles.find(isTheImage);
    let img = document.createElement("img");
    img.src = theImage.name;
    img.alt = dogSelect.value;
    img.size = 300;
    myDiv.appendChild(img);
    return false;
}
function isTheImage(inArray) {
    const dogSelect = document.getElementById("dogs");
    if (inArray.description == dogSelect.value) {
        return true;
    }
    return false;
}

function onclearImagesClicked() {
    const myDiv = document.getElementById("myDiv");
    const allImages = document.querySelectorAll("img");
    for (let i = 0; i < allImages.length; i++) {
        myDiv.removeChild(allImages[i]);
    }

}