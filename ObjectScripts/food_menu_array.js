let menu = [
    [
        { item: "Sausage and Egg Biscuit", price: 3.69 },
        { item: "Bacon and Egg Biscuit", price: 3.49 },
        { item: "Ham and Egg Biscuit", price: 3.29 }
    ],
    [
        { item: "Burger and Fries", price: 8.49 },
        { item: "Veggie Burger and Fries", price: 9.69 },
        { item: "Turkey wrap and carrot slices", price: 6.49 },
        { item: "One topping mini cheese pizza", price: 3.29 }
    ],
    [
        { item: "Burger and Fries", price: 10.49 },
        { item: "Veggie Burger and Fries", price: 11.69 },
        { item: "Steak and Potato", price: 15.49 },
        { item: "King crab and salad", price: 25.49 },
        { item: "Chicken and veggies", price: 11.29 }
    ]
];

let meal = [0, 1, 2];
let type = ["Breakfast Menu", "Lunch Menu", "Dinner Menu"];

let numMeals = meal.length;
for (let i = 0; i < numMeals; i++) {
    console.log("-----------");
    console.log(type[i]);
    console.log("-----------");
    let numEntrees = menu[i].length;
    for (let j = 0; j < numEntrees; j++) {
        console.log(menu[i][j]);
    }
}