let academyMembers = [
    {
        memID: 101,
        name: "Bob Brown",
        films: ["Bob I", "Bob II", "Bob III", "Bob IV"]
    },
    {
        memID: 142,
        name: "Sallie Smith",
        films: ["A Good Day", "A Better Day"]
    },
    {
        memID: 187,
        name: "Fred Flanders",
        films: ["Who is Fred?", "Where is Fred?",
            "What is Fred?", "Why Fred?"]
    },
    {
        memID: 203,
        name: "Bobbie Boots",
        films: ["Walking Boots", "Hiking Boots",
            "Cowboy Boots"]
    },
];

function getFilmBeginningA(name, films) {
    let matching = [];
    let numFilms = films.length;
    for (let i = 0; i < numFilms; i++) {
        if (films[i].substr(0, 1) == "A") {
            matching.push(name);
        }
    }
    for (let i = 0; i < matching.length; i++) {
        console.log("Actors with films beginning A",matching.name);
    }
    return matching;
}

let numActors = academyMembers.length;
for (let i = 0; i < numActors; i++) {
    if (academyMembers[i].memID == 187) {
        console.log("Name of mem ID 187:", academyMembers[i].name);
    }
    if (academyMembers[i].films.length >= 3) {
        console.log("Name of actor in 3 films or more:", academyMembers[i].name);
    }
    if (academyMembers[i].name.substr(0, 3) == "Bob") {
        console.log("Name of actor that begins Bob:", academyMembers[i].name);
    }
    getFilmBeginningA(academyMembers[i].name, academyMembers[i].films);
}