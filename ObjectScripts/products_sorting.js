let products = [
    { product: "Gummy Worms", price: 5.79 },
    { product: "Plain M&Ms", price: 2.89 },
    { product: "Peanut M&Ms", price: 2.89 },
    { product: "Swedish Fish", price: 3.79 },
    { product: "Snickers", price: 1.25 },
    { product: "Junior Mint", price: 1.79 },
    { product: "Dots", price: 0.79 },
    { product: "Hersheys Chocolate", price: 4.79 },
    { product: "Extra Gum", price: 5.79 },

];
products.sort(function (a, b) {
    if (a.product < b.product) return -1;
    else if (a.product == b.product) return 0;
    else return 1;
});

let numProducts = products.length;
for (let i = 0; i < numProducts; i++) {
    console.log(products[i].product);
}
console.log("---------------------------------------------------")
products.sort(function (a, b) {
    if (a.product > b.product) return -1;
    else if (a.product == b.product) return 0;
    else return 1;
});
for (let i = 0; i < numProducts; i++) {
    console.log(products[i].product);
}