let f1 = greet => console.log("Hi!");
f1();

let square = x => x * x;
console.log(`The square of 8 is ${square(8)}`);

let isPositive = x => x >= 0;
console.log(`Is negative one positive: ${isPositive(-1)}`);
console.log(`Is five positive: ${isPositive(5)}`);

let subtract = (x, y) => x - y;
console.log(`one hundred minuse seventy five is ${subtract(100, 75)}`);

let biggestOfTwo = (x, y) => {
    if (x > y) {
        return x
    }
    else if (x < y) {
        return y
    }
}
console.log(`The biggest of 32 and 64 is ${biggestOfTwo(32,64)}`);

let findBiggest = arr => {
    let biggest = arr[0];
    for (let i = 1; i < arr.length; i++) {
        if (biggest < arr[i]) {
            biggest = arr[i];
        }
    }
    return biggest;
}
console.log(`The biggest of the array is ${findBiggest([12, 15, 25, 2, 5, 55, 8 , 33])}`);
