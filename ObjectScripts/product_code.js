function parsePartCode(part) {
    let pos1 = part.indexOf(":");
    let pos2 = part.lastIndexOf("-");
    let supplierName = part.substr(0,pos1);
    let prodNum = part.substring(pos1+1,pos2);
    let prodSize = part.substring(pos2+1);
    let partCode = {
        supplierCode: supplierName,
        productNumber: prodNum,
        size: prodSize
    }
    return partCode;
}

let partCode1 = "XYZ:1234-L";
let part1 = parsePartCode(partCode1);
console.log("Supplier: " + part1.supplierCode +
    " Product Number: " + part1.productNumber +
    " Size: " + part1.size);