let lunch = [
    { item: "Steak Fajitas", price: 9.95 },
    { item: "Chips and Guacamole", price: 5.25 },
    { item: "Sweet Tea", price: 2.79 }
];

let lunchTotal = 0;
let numLunch = lunch.length;
for (let i = 0; i < numLunch; i++) {
    lunchTotal += lunch[i].price;
}
lunchTotal += (lunchTotal * .18)
lunchTotal += 8;
console.log(lunchTotal.toFixed(2));
