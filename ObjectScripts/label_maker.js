let person = {
    name: "Chuck Borst",
    address: "1 CM",
    city: "Detroit",
    state: "MI",
    zip: "48226"
}
printContact(person);

function printContact(person) {
    // for (let key in person) {
    //     console.log(key + " = " + person[key]);
    // }

    console.log(person.name);
    console.log(person.address);
    console.log(person.city + ", " + person.state + " " + person.zip);

}