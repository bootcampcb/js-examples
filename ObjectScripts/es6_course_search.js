let courses = [
    {
        CourseId: "PROG100",
        Title: "Introduction to HTML/CSS/Git",
        Location: "Classroom 7",
        StartDate: "09/08/22",
        Fee: "100.00",
    },
    {
        CourseId: "PROG200",
        Title: "Introduction to JavaScript",
        Location: "Classroom 9",
        StartDate: "11/22/22",
        Fee: "350.00",
    },
    {
        CourseId: "PROG300",
        Title: "Introduction to Java",
        Location: "Classroom 1",
        StartDate: "01/09/23",
        Fee: "50.00",
    },
    {
        CourseId: "PROG400",
        Title: "Introduction to SQL and Databases",
        Location: "Classroom 7",
        StartDate: "03/16/23",
        Fee: "50.00",
    },
    {
        CourseId: "PROJ500",
        Title: "Introduction to Angular",
        Location: "Classroom 1",
        StartDate: "04/25/23",
        Fee: "50.00",
    }
];

function isProg200(inArray) {
    if (inArray.CourseId == "PROG200") {
        return true;
    }
    return false;
}
function isProj500(inArray) {
    if (inArray.CourseId == "PROJ500") {
        return true;
    }
    return false;
}
function class50(inArray) {
    if (Number(inArray.Fee) <= 50) {
        return true;
    }
    return false;
}
function class1filter(inArray) {
    if (inArray.Location == "Classroom 1") {
        return true;
    }
    return false;
}

let prog200 = courses.find(isProg200);
console.log(`PROG 200 Start Date: ${prog200.StartDate}`);

let proj500 = courses.find(isProj500);
console.log(`\nPROJ500 Title: $(proj500.Title)`);

let classesLessThan50 = courses.filter(class50);
console.log('\nClasses 50 dollars or less:');
console.log(classesLessThan50);

let class1 = courses.filter(class1filter);
console.log('\nCourses in classroom 1:')
console.log(class1);