"use strict";

window.onload = init;

function init() {
    const addBtn = document.getElementById("addBtn");
    const subtractBtn = document.getElementById("subtractBtn");
    const multiplyBtn = document.getElementById("multiplyBtn");
    const divideBtn = document.getElementById("divideBtn");

    addBtn.onclick = onAddBtnClicked;
    subtractBtn.onclick = onSubtractBtnClicked;
    multiplyBtn.onclick = onMultiplyBtnClicked;
    divideBtn.onclick = onDivideBtnClicked;

}

function onAddBtnClicked() {
    const num1 = document.getElementById("num1Field");
    const num2 = document.getElementById("num2Field");
    const answerField = document.getElementById("answerField");
    let answer = Number(num1.value) + Number(num2.value);

    answerField.value = answer;
}

function onSubtractBtnClicked() {
    const num1 = document.getElementById("num1Field");
    const num2 = document.getElementById("num2Field");
    const answerField = document.getElementById("answerField");
    let answer = Number(num1.value) - Number(num2.value);

    answerField.value = answer;
}

function onMultiplyBtnClicked() {
    const num1 = document.getElementById("num1Field");
    const num2 = document.getElementById("num2Field");
    const answerField = document.getElementById("answerField");
    let answer = Number(num1.value) * Number(num2.value);

    answerField.value = answer;
}

function onDivideBtnClicked() {
    const num1 = document.getElementById("num1Field");
    const num2 = document.getElementById("num2Field");
    const answerField = document.getElementById("answerField");
    let answer = Number(num1.value) / Number(num2.value);

    answerField.value = answer;
}