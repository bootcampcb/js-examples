
"use strict";

window.onload = init;

function init() {
    const msgPs = document.getElementsByTagName("p");
    const imageTags = document.getElementsByTagName("img");
    // for (let i = 0; i < msgPs.length; i++) {
    //     msgPs[i].style.border = "8px solid red";
    // }

    Array.from(msgPs).forEach((element) => {
        element.style.border = "1px solid black";
    });

    Array.from(imageTags).forEach((element) => {
        element.classList.add("roundedImg");
    });

}


